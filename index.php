<?php

require "vendor/autoload.php";
use CrudKit\CrudKitApp;
use CrudKit\Pages\MySQLTablePage;

// Create a new app
$app = new CrudKitApp();
$app->setStaticRoot ("static/crudkit/");


// Create a new page mapped to a table
$sqlPage = new MySQLTablePage ("tabla_clientes", "root", "", "3dprint");
$sqlPage->setName("Clientes");
$sqlPage->setTableName ("clientes");  // Set the table name
$sqlPage->setPrimaryColumn ("cli_id");
$sqlPage->addColumn ("cli_nombre", "Nombre");
$sqlPage->addColumn ("cli_apellido", "Apellido");
$sqlPage->addColumn ("cli_direccion", "Direccion");
$sqlPage->addColumn ("cli_mail", "Email");
$sqlPage->addColumn ("cli_fecha_alta", "Fecha de alta");
$sqlPage->setSummaryColumns (array("cli_nombre","cli_apellido","cli_direccion","cli_mail", "cli_fecha_alta"));
$app->addPage($sqlPage);




$sqlPage2 = new MySQLTablePage ("tabla_productos", "root", "", "3dprint");
$sqlPage2->setName("Productos");
$sqlPage2->setTableName ("productos");  // Set the table name
$sqlPage2->setPrimaryColumn ("prod_id");
$sqlPage2->addColumn ("prod_descripcion", "Descripcion");
$sqlPage2->addColumn ("prod_pesoconresiduo", "Peso con residuo");
$sqlPage2->addColumn ("prod_pesosinresiduo", "Peso sin residuo");
$sqlPage2->addColumn ("prod_imagen", "Ubicacion de imagen");
$sqlPage2->addColumn ("prod_preciocosto", "Precio de costo");
$sqlPage2->addColumn ("prod_precioventa", "Precio de venta");
$sqlPage2->addColumn ("prod_fechaalta", "Fecha de alta del producto");
$sqlPage2->addColumn ("prod_stock", "Stock");
$sqlPage2->setSummaryColumns (array("prod_descripcion","prod_pesoconresiduo","prod_pesosinresiduo","prod_imagen","prod_preciocosto","prod_precioventa","prod_fechaalta","prod_stock"));

// Add the page to the app
$app->addPage($sqlPage2);





$sqlPage3 = new MySQLTablePage ("tabla_pedidos", "root", "", "3dprint");
$sqlPage3->setName("Pedidos");
$sqlPage3->setTableName ("pedidos");  // Set the table name
$sqlPage3->setPrimaryColumn ("ped_id");
$sqlPage3->addColumn ("ped_fechapedido", "Fecha del pedido");
$sqlPage3->addColumn ("ped_fechaentrega", "Fecha de entrega");
$sqlPage3->addColumn ("ped_entregado", "Entregado Si/No");
$sqlPage3->addColumn ("ped_cliente", "Cliente");
$sqlPage3->setSummaryColumns (array("ped_fechapedido","ped_fechaentrega","ped_entregado","ped_cliente"));

// Add the page to the app
$app->addPage($sqlPage3);




$sqlPage4 = new MySQLTablePage ("detalle_pedidos", "root", "", "3dprint");
$sqlPage4->setName("Detalle Pedidos");
$sqlPage4->setTableName ("detalle_pedido");  // Set the table name
$sqlPage4->setPrimaryColumn ("det_idpedido");
$sqlPage4->addColumn ("det_idproducto", "Fecha del pedido");
$sqlPage4->addColumn ("det_preciounidad", "Fecha de entrega");
$sqlPage4->addColumn ("det_cantidad", "Entregado Si/No");
$sqlPage4->setSummaryColumns (array("det_idproducto","det_preciounidad","det_cantidad"));

// Add the page to the app
$app->addPage($sqlPage4);


// Render the app
$app->render();
?>
